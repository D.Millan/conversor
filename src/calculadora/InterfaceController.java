package calculadora;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

/**
 *
 * @author Daniel 2
 */
public class InterfaceController implements Initializable {
    
    double answer;
    double dif;
    
    @FXML
    private TextField entrada;
    
    
    @FXML
    private void converse1(ActionEvent event) {
        answer = Double.parseDouble(entrada.getText())*(9.0/5) + 32;
        answer = Math.ceil(answer*1000)/1000;
        entrada.setText("" + answer);
    }
    
    @FXML
    private void converse2(ActionEvent event) {
        answer = (Double.parseDouble(entrada.getText())+273.15);
        answer = Math.ceil(answer*1000)/1000;
        entrada.setText("" + answer);
    }
    
    @FXML
    private void converse3(ActionEvent event) {
        answer = (Double.parseDouble(entrada.getText())-32)*5.0/9;
        answer = Math.ceil(answer*1000)/1000;
        entrada.setText("" + answer);
    }
    
    @FXML
    private void converse4(ActionEvent event) {
        answer = (Double.parseDouble(entrada.getText())-32)*5.0/9 + 273.15;
        answer = Math.ceil(answer*1000)/1000;
        entrada.setText("" + answer);
    }
    
    @FXML
    private void converse5(ActionEvent event) {
        answer = (Double.parseDouble(entrada.getText())-273.15)*(9.0/5) + 32;
        answer = Math.ceil(answer*1000)/1000;
        entrada.setText("" + answer);
    }
    @FXML
    private void converse6(ActionEvent event) {
        answer = Double.parseDouble(entrada.getText())-273.15;
        answer = Math.ceil(answer*1000)/1000;
        entrada.setText("" + answer);
    }
    
    @FXML
    private void add(ActionEvent event)
    {
        Button b = (Button) event.getTarget();
        entrada.setText(entrada.getText() + b.getText());
    }
    
    @FXML
    private void Minus(ActionEvent event)
    {
        if("".equals(entrada.getText()))
        {
            entrada.setText("-");
        }
        else if("-".equals(entrada.getText()))
        {
            entrada.setText("");
        }
        else
        {
            try{
                entrada.setText("" + (0 - Integer.parseInt(entrada.getText())));
            }

            catch (Exception erro){
                entrada.setText("" + (0 - Double.parseDouble(entrada.getText())));
            }
        }
    }
    
    @FXML
    private void entradaClear()
    {
        entrada.setText("");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
